import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { DialogService } from '../dialog.service';


@Component({
  selector: 'app-all-products',
  templateUrl: './all-products.component.html',
  styleUrls: ['./all-products.component.scss']
})
export class AllProductsComponent  {

  constructor(private router: Router,private route: ActivatedRoute,
    private auth:AuthService,private httpClient:HttpClient, private dialogService:DialogService) { }

  httpdata:any;
  ngOnInit() {
    
    this.getProduct()
  }
  createProduct(){   
    this.router.navigate(['/createProduct']);
  }

  editProduct(){   
    this.router.navigate(['/editProduct']);
  }
  
  getProduct(){
    this.httpClient.get<any>('https://fakestoreapi.com/products').subscribe((data)=>{
      this.httpdata=data;
    console.log("::data",this.httpdata)})
  }

 
  confirmDialog(){
    this.dialogService.openConfirmDialog();
    
  }
}





 