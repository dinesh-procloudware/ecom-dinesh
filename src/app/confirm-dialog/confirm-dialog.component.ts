import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(private auth:AuthService,private httpClient:HttpClient
    ,private _snackBar: MatSnackBar) { }
    httpdata:any;
  ngOnInit(): void {
  }

deleteProduct(){
  console.log("::Delete Product");
  this.httpClient.delete<any>('https://fakestoreapi.com/products/6').subscribe((data)=>{
    this.httpdata=data;
  console.log("::data",this.httpdata);
  this._snackBar.open('Product Deleted','',{
    duration:2000,
    verticalPosition:'bottom'
  });
})
}
}
