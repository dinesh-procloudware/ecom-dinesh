import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {MatBadgeModule} from '@angular/material/badge';
import {MatToolbarModule} from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { LoginComponent } from './login/login.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {MatButtonModule} from '@angular/material/button';
import { AllProductsComponent } from './all-products/all-products.component';  
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import { CreateNewProductComponent } from './create-new-product/create-new-product.component';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { EditPageComponent } from './edit-page/edit-page.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AllProductsComponent,
    CreateNewProductComponent,
    ConfirmDialogComponent,
    EditPageComponent
  ],
  imports: [MatSnackBarModule,MatCardModule,MatDialogModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatSliderModule,MatBadgeModule,
    MatToolbarModule,
    MatIconModule,MatFormFieldModule,MatInputModule,
    ReactiveFormsModule,MatButtonModule,FlexLayoutModule
   
  
  ],
  exports: [
    MatButtonModule, MatDialogModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[ConfirmDialogComponent]
})
export class AppModule { }
