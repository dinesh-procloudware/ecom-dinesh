import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import {MatSnackBar} from '@angular/material/snack-bar';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-create-new-product',
  templateUrl: './create-new-product.component.html',
  styleUrls: ['./create-new-product.component.scss']
})
export class CreateNewProductComponent  {
  formGroup!: FormGroup;   
  categoryError: string = 'Enter category';
  alertPrductTitle:string='Enter Title';
  alertAmount:string='Enter Price';
  alertDescription:string='Enter Description';
  imageError:string='Enter image Url'


  constructor(private router: Router,private route: ActivatedRoute,private formBuilder: FormBuilder,private http:HttpClient,
    private _snackBar: MatSnackBar, private auth:AuthService ) { }

    ngOnInit() {
      this.createForm();
     
    }
    allProduct(){
      console.log("Dinesh")
      this.router.navigate(['/allProducts']);
    }
  
    createForm() {     
      this.formGroup = this.formBuilder.group({       
        'category': [null, Validators.required],
        ' title': [null, Validators.required,],
        'price':[null,Validators.required],
        'description':[null,Validators.required] ,
        'image':[null,Validators.required]       
      });
    } 
   
  
   
    get  title() {
      return this.formGroup.get(' title') as FormControl
    } 
    get price() {
      return this.formGroup.get('price') as FormControl
    } 
  
    get description() {
      return this.formGroup.get('description') as FormControl
    } 

    get category() {
      return this.formGroup.get('category') as FormControl
    } 
    get image() {
      return this.formGroup.get('image') as FormControl
    } 
    
    onSubmit() {      
      if( this.formGroup.valid){
        console.log("::Dinesh")
        this.auth.insertProduct(this.formGroup.value).subscribe((result)=>{ console.log("resullt",result);
          if(result.id){
            console.log("resullt",result);
            this._snackBar.open('Product Created','',{
              duration:2000,
              verticalPosition:'bottom',
              
            }
           
            );  
             this.allProduct(); 
                     
         }
         
          else{         
            this._snackBar.open(result.msg.toString(),'',{
              duration:2000,
              verticalPosition:'bottom'
            });        
          }
         
        })
      }
    
    }
  
  }


