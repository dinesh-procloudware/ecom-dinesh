import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllProductsComponent } from './all-products/all-products.component'
 import { LoginComponent }  from './login/login.component'
 import { CreateNewProductComponent } from './create-new-product/create-new-product.component'
import { EditPageComponent } from './edit-page/edit-page.component';
const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'allProducts', component: AllProductsComponent },
  { path: 'createProduct', component: CreateNewProductComponent },
  { path: 'editProduct', component: EditPageComponent },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
 
})
export class AppRoutingModule { }
