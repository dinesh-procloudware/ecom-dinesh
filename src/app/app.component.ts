import { Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'eCom';
  constructor(private route: ActivatedRoute, private router: Router ) { }
  navigate(path: any) {
    this.router.navigate([path]);
  }
}
