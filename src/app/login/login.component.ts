import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent  {
 
  hide = true;
  formGroup!: FormGroup;   
  titleAlert: string = 'This field is required';
  post: any = '';

  constructor(private formBuilder: FormBuilder,private http:HttpClient,private router: Router,
    private auth:AuthService,private _snackBar: MatSnackBar) { 
 
  }
  ngOnInit() {
    this.createForm();   
  }

  createForm() {
    this.formGroup = this.formBuilder.group({     
      'username': [null, Validators.required],
      'password': [null, Validators.required],
     });
  }
  get username() {
    return this.formGroup.get('username') as FormControl
  }
  get password() {
    return this.formGroup.get('password') as FormControl
  } 
 
  createProduct(){   
    this.router.navigate(['/allProducts']);
  }

    onSubmit() {    
    //  let data={ username: "mor_2314",
    // password: "83r5^_"};     
    if( this.formGroup.valid){
      this.auth.login(this.formGroup.value).subscribe((result)=>{ console.log("resullt",result);
        if(result.token){
          console.log("resullt",result);
          this._snackBar.open('Login SuccessFully','',{
           duration:2000,
           verticalPosition:'bottom'
          });this.createProduct();           
        }else{         
          this._snackBar.open(result.msg.toString(),'',{
            duration:2000,
            verticalPosition:'bottom'
          });        
        }})
    }
  
  }

}